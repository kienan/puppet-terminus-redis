This module provides a redis terminus for storing configuration from puppet.

A redis instance can be used for caching facts (via routes.yaml), and/or as a
backend for storeconfigs. When used as such a backend, PuppetDB is no longer
required in order to use exported resources.

# Pre-requisites

redis running on localhost, on the default port

# Installation

1. Install the ruby files:

    cp -r src/lib/* /usr/lib/ruby/vendor_ruby

2. Update routes, `/etc/puppet/routes.yaml`: eg.,

```
---
master:
  facts:
    terminus: redis
    cache: yaml
```

3. In the `/etc/puppet/puppet.conf` master section:

```
[master]
    storeconfigs = true
    storeconfigs_backend = redis
```

4. Restart the puppet server or rack application, eg.

```
service apache2 restart
```

# License

GPLv3
