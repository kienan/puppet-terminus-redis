require 'puppet/resource/catalog'
require 'puppet/indirector/rest'
require 'redis'

class Puppet::Resource::Catalog::Redis < Puppet::Indirector::REST

  def save(request)
    redis = Redis.new
    environment = request.options[:environment] || request.environment.to_s
    redis.set "catalog_#{request.key}_#{environment}", request.instance.to_json
  end

  def find(request)
    nil
  end
end
