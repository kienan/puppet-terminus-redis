require 'puppet/node/facts'
require 'puppet/indirector/rest'
require 'json'
require 'redis'

class Puppet::Node::Facts::Redis < Puppet::Indirector::REST
  def get_trusted_info(node)
    trusted = Puppet.lookup(:trusted_information) do
      Puppet::Context::TrustedInformation.local(node)
    end
    trusted.to_h
  end

  def delete(request)
    redis = Redis.new
    environment = request.options[:environment] || request.environment.to_s
    key = "node_facts_#{request.key}_#{environment}"
    redis.del key
  end
  def find(request)
    redis = Redis.new
    environment = request.options[:environment] || request.environment.to_s
    key = "node_facts_#{request.key}_#{environment}"
    values = redis.get key
    print values
    Puppet::Node::Facts.new(request.key, values)
  end

  def save(request)
    redis = Redis.new
    environment = request.options[:environment] || request.environment.to_s
    request.instance.values.merge(get_trusted_info(request.node))
    facts = request.instance.dup
    facts.values = facts.values.dup
    facts.values[:trusted] = get_trusted_info(request.node)
    key = "node_facts_#{facts.name}_#{environment}"
    redis.set key, facts.values.to_json
  end
end
