require 'puppet/indirector/rest'
require 'json'
require 'redis'

class Puppet::Resource::Redis < Puppet::Indirector::REST
  def search(request)
    redis = Redis.new
    host = request.options[:host]
    type = request.key
    # I'm not really sure what filter might be
    # In the puppetdb terminus it gets append to the default query
    # (which is detailed below)
    # filter = request.options[:filter]
    scope = request.options[:scope]
    environment = request.options[:environment] || request.environment.to_s

    # In the puppetdb storeconfigs terminus, the query is
    # expr = ['and',
    #  ['=', 'type', type],
    #  ['=', 'exported', true],
    #  ['not',
    #    '=', 'certname', host]]]
    #
    # This means that it gets all exported resources of that type
    # from all hosts that aren't the request's host.
    # Since we sort a catalog entry per host, we need to check all
    # keys matching "catalog_*_#{environment}"
    # excluding "catalog_#{host}_#{enviroment}".
    # This means we will have to make 1 request for the keys,
    # and then n-1 requests.
    #
    key = "catalog_#{host}_#{environment}"
    resources = []
    keys = redis.keys("catalog_*_#{environment}")
    keys.each do |k|
      next if k == key
      catalog = JSON.parse(redis.get(k))
      catalog['resources'].each do |res|
        # Skip resources that don't match the type
        next if res['type'] != type
        # Skip non-exported resources
        next if not res['exported']
        params = res['parameters'] || {}
        params = params.map do |name, value|
          Puppet::Parser::Resource::Param.new(:name => name, :value => value)
        end
        attrs = {:parameters => params, :scope => scope}
        t = res['type']
        t = Puppet::Pops::Evaluator::Runtime3ResourceSupport.find_resource_type(scope, t) unless t == 'class' || t == 'node'
        result = Puppet::Parser::Resource.new(t, res['title'], attrs)
        result.collector_id = "#{res['certname']}|#{res['type']}|#{res['title']}"
        resources.append(result)
      end
    end
    resources
  end
end
